﻿using Logger.Abstract;
using Logger.Concrete;
using Logger.Models;
using System;

namespace Logger
{
    class Program
    {
        static void Main(string[] args)
        {
            //NEW FEATURE
            SessionLogger logger = new();

            //NEW FEATURE
            UserAccount user = new()
            {
                Id = 1,
                Login = "User1"
            };
            
            ISession sessionUser = new Session(user);
            
            logger.LogSession(sessionUser);
            
            //NEW FEATURE
            AdminAccount admin = new()
            {
                Id = 2,
                Login = "Admin1",
                IsSuperAdmin = true
            };
            //admin.IsSuperAdmin = false;

            //NEW FEATURE
            Session sessionAdmin = new(admin);

            logger.LogSession(sessionAdmin);

            Console.ReadKey();
        }
    }
}
