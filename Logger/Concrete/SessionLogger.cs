﻿using Logger.Abstract;
using Logger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger.Concrete
{
    public class SessionLogger : ILogger
    {
        public void LogSession(ISession session)
        {
            var account = session.GetAccount();

            //NEW FEATURE
            if (account is UserAccount)
            {
                Console.WriteLine($"{account}");
            }
            else
            {
                ConsoleColor colorPrev = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{account}");
                Console.ForegroundColor = colorPrev;
            }
        }
    }
}
