﻿using Logger.Abstract;
using Logger.Models;

namespace Logger.Concrete
{
    /// <summary>
    /// Класс сессии (часть 2)
    /// </summary>
    public partial class Session : ISession
    {
        //NEW FEATURE
        public partial BaseAccount GetAccount()
        {
            return _account;
        }
    }
}
