﻿using Logger.Abstract;
using Logger.Models;
using System;

namespace Logger.Concrete
{
    /// <summary>
    /// Класс сессии (часть 1)
    /// </summary>
    public partial class Session : ISession
    {
        private BaseAccount _account;

        public Session(BaseAccount account)
        {
            DateCreate = DateTime.Now;
            _account = account;
        }

        public DateTime DateCreate { get; }

         //NEW FEATURE
        public partial BaseAccount GetAccount();
    }
}
