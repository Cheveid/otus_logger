﻿namespace Logger.Models
{
    /// <summary>
    /// Аккаунт администратора
    /// </summary>
    public class AdminAccount : BaseAccount
    {
        /// <summary>
        /// Признак супер-администратора
        /// </summary>
        //NEW FEATURE
        public bool IsSuperAdmin { get; init; }

        public override string ToString()
        {
            return $"AdminAccount: Id={Id}, Login={Login}, IsSuperAdmin={IsSuperAdmin}";
        }
    }
}
