﻿namespace Logger.Models
{
    /// <summary>
    /// Аккаунт пользователя
    /// </summary>
    public class UserAccount : BaseAccount
    {
        public override string ToString()
        {
            return $"UserAccount: Id={Id}, Login={Login}";
        }
    }
}
