﻿namespace Logger.Models
{
    /// <summary>
    ///  Базовый аккаунт
    /// </summary>
    public abstract class BaseAccount
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        //NEW FEATURE
        public int Id { get; init; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        public override string ToString()
        {
            return $"BaseAccount: Id={Id}, Login={Login}";
        }
    }
}
