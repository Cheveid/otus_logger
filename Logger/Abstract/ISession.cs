﻿using Logger.Models;
using System;

namespace Logger.Abstract
{
    /// <summary>
    /// Интерфейс сессии
    /// </summary>
    public interface ISession
    {
        /// <summary>
        /// Дата создания сессии
        /// </summary>
        public DateTime DateCreate { get; }

        /// <summary>
        /// Возвращает текущий аккаунт
        /// </summary>
        /// <returns></returns>
        public BaseAccount GetAccount();
    }
}
